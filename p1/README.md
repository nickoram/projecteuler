Problem 1 - Multiples of 3 and 5
==============

Problem
--------------
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.

Answer
--------------
233168

Instructions
--------------
```
javac Problem.java
javac -cp .:junit-4.12.jar:hamcrest-core-1.3.jar TestProblem.java
java -cp .:junit-4.12.jar:hamcrest-core-1.3.jar org.junit.runner.JUnitCore TestProblem
```
