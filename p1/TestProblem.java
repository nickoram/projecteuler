import org.junit.Test;
import static org.junit.Assert.*;

public class TestProblem {

    public static void main(String args[]) {
        Problem problem = new Problem();
        System.out.println("f(1000) = " + problem.solve(1000));
    }

    @Test
    public void testNonNegativeInteger() {
        int n = -1;
        System.out.println("Test non-positive integer, f(-1) = 0");

        Problem problem = new Problem();
        assertTrue(0 == problem.solve(n));
    }

    @Test
    public void testZero() {
        int n = 0;
        System.out.println("Test zero, f(0) = 0");

        Problem problem = new Problem();
        assertTrue(0 == problem.solve(0));
    }

    @Test
    public void testBaseCase() {
        int n = 10;
        System.out.println("Test base case, f(10) = 23");

        Problem problem = new Problem();
        assertTrue(23 == problem.solve(10));
    }

    @Test
    public void testProjectEuler() {
        int n = 1000;
        System.out.println("Test project euler, f(1000) = 233168");

        Problem problem = new Problem();
        assertTrue(233168 == problem.solve(1000));
    }
}
