/**
 * Multiples of 3 and 5
 * <p/>
 * If we list all the natural numbers below 10 that are multiples of 3
 * or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */
public class Problem {

    public int solve(int n) {
        if (n <= 1) {
            return 0;
        }

        return recurse(n - 1);
    }

    private int recurse(int n) {

        if (n == 1) {
            return 0;
        }

        if (n % 3 == 0) {
            return n + recurse(n - 1);
        }

        if (n % 5 == 0) {
            return n + recurse(n - 1);
        }

        // skip this crappy number
        return recurse(n - 1);
    }

}